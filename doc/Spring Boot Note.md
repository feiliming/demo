# Spring Boot Note

### 生成项目结构

1. 访问https://start.spring.io/
2. 输入包名、项目名及依赖，依赖选择Web、MySQL、MyBatis、DevTools
3. 点击Generate Project生成项目，并导出压缩包
4. 在码云创建一个项目demo，然后Clone到本地git目录
5. 将压缩包内容解压到demo目录，两个.gitignore内容合并
6. 导入项目到eclipse

### 配置数据源

1. 引入阿里巴巴druid数据库连接池
2. 建一个数据库，demo
3. 新建application-db.properties，注意文件编码使用UTF-8，配置数据库信息
	spring.datasource.type=com.alibaba.druid.pool.DruidDataSource
	spring.datasource.driver-class-name=com.mysql.jdbc.Driver
	spring.datasource.url=jdbc:mysql://localhost:3306/demo?characterEncoding=utf8
	spring.datasource.username=root
	spring.datasource.password=root

### 配置web容器信息

1. 新建application-app.properties，注意文件编码使用UTF-8，配置Tomcat信息
	server.port=8090
	server.address=127.0.0.1
	server.sessionTimeout=30
	server.contextPath=/demo
	server.tomcat.uri-encoding=UTF-8
2. 在application.properties中引入上面两个properties
	spring.profiles.active=app,db
	或者spring.profiles.include=app,db
	
### 启动程序

1. 使用是内嵌Tomcat，main函数入口在DemoApplication.java，右键Run as -》 Java Application